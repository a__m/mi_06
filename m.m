function Y = m(x)
    Y = [];
    for xi = x
        Y(end+1) = atan(2*xi)^2;
    end
end