function [X, Y] = nonlinearSystemCauchy(N)
    X = -pi + (pi + pi) * rand(1, N);
    Y = m(X) + randc(N);
end