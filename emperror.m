function err = emperror(Q, k, alfaKValues)
    err = 0;
    for q = -Q:0.1:Q
        xq = q;
        err = err + (m(xq) - mEstimated(q, k, alfaKValues))^2;
    end
end