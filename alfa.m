function alfaK = alfa(X, Y, k)
    X0 = -pi;
    N = length(X);
    F = @(x_)cos(k*x_); 
    alfaK = Y(1) * 1/pi * quad(F, X0, X(1));
%     alfaK = Y(1) * (sin(k*X(1))/k - sin(k*X0)/k);
    for n = 2:N
%         alfaK = alfaK + (Y(n) * (sin(k*X(n))/k - sin(k*X(n-1))/k));
        alfaK = alfaK + Y(n) * 1/pi * quad(F, X(n-1), X(n));
    end
end