function est = mEstimated(x, K, alfaKValues)
    est = alfaKValues(0) / 2;
    for k = 1:1:K
        est = est + (alfaKValues(k) * cos(k*x));
    end
end