function random_cauchy = randc(N)
    u_c = rand(N, 1);
    random_cauchy = [];
    gamma = 1;
    for n = 1:1:N
        random_cauchy(n) = gamma * tan(pi * (u_c(n) - 1/2));
    end
end