clear all; close all;


%%%%%%%%%%%% Gauss %%%%%%%%%%%%%

[X1024 Y1024] = nonlinearSystemGauss(1024);
figure();
plot(X1024, Y1024, '.');
hold on;
estimate(X1024, Y1024);

indices512 = randsample(1:1:1024, 512);
X512 = X1024(indices512);
Y512 = Y1024(indices512);
[X512, sortIndex] = sort(X512);
Y512 = Y512(sortIndex);
figure();
plot(X512, Y512, '.');
hold on;
estimate(X512, Y512);

indices256 = randsample(1:1:1024, 256);
X256 = X1024(indices256);
Y256 = Y1024(indices256);
[X256, sortIndex] = sort(X256);
Y256 = Y256(sortIndex);
figure();
plot(X256, Y256, '.');
hold on;
estimate(X256, Y256);

indices128 = randsample(1:1:1024, 128);
X128 = X1024(indices128);
Y128 = Y1024(indices128);
[X128, sortIndex] = sort(X128);
Y128 = Y128(sortIndex);
figure();
plot(X128, Y128, '.');
hold on;
estimate(X128, Y128);

%%%%%%%%%%% Cauchy %%%%%%%%%%%%%%%

[X1024 Y1024] = nonlinearSystemCauchy(1024);
figure();
plot(X1024, Y1024, '.');
hold on;
estimate(X1024, Y1024);

indices512 = randsample(1:1:1024, 512);
X512 = X1024(indices512);
Y512 = Y1024(indices512);
[X512, sortIndex] = sort(X512);
Y512 = Y512(sortIndex);
figure();
plot(X512, Y512, '.');
hold on;
estimate(X512, Y512);

indices256 = randsample(1:1:1024, 256);
X256 = X1024(indices256);
Y256 = Y1024(indices256);
[X256, sortIndex] = sort(X256);
Y256 = Y256(sortIndex);
figure();
plot(X256, Y256, '.');
hold on;
estimate(X256, Y256);

indices128 = randsample(1:1:1024, 128);
X128 = X1024(indices128);
Y128 = Y1024(indices128);
[X128, sortIndex] = sort(X128);
Y128 = Y128(sortIndex);
figure();
plot(X128, Y128, '.');
hold on;
estimate(X128, Y128);

