function [estimated] = GMEstimator(X, K, alfaKValues)
    N = length(X);
    estimated = zeros(1, N);
    for n = 1:N
        estimated(n) = mEstimated(X(n), K, alfaKValues);
    end
end