function [X, Y] = nonlinearSystemGauss(N)
    X = -pi + (pi + pi) * rand(1, N);
    Y = m(X) + randn(1, N);
end