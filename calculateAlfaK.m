function alfaKValues = calculateAlfaK(X, Y, K)
    alfaKValues = containers.Map([1025], [0]);
    for k = 0:1:K
        alfaKValues(k) = alfa(X, Y, k);
    end
end