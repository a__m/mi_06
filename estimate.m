function estimate(X, Y)
    [X, sortIndex] = sort(X);
    Y = Y(sortIndex);
    
    N = length(X);
    
    bestErr = Inf;
    bestK = 0;

    kParameters = 1:1:sqrt(N);
    alfaKValues = calculateAlfaK(X, Y, kParameters(end));
    for k = kParameters
        err = emperror(pi, k, alfaKValues);
        if (err < bestErr)
            bestErr = err;
            bestK = k;
        end
    end

    bestErr
    bestK
    estimated = GMEstimator(X, bestK, alfaKValues);

    plot(X, estimated, '.');
    hold off;
    title(['N = ' num2str(N) ', k = ' num2str(bestK)]);
    legend('Wygenerowany system', 'Estymowany system');
    xlabel('Xn');
    ylabel('Yn');
end